# README #
Name: Contains Automatic Custom Topology Generator tool and Enhanced Automatic Configuration of RouteFow (EACRF).

This software automatically generates the virtual environment proposed for RouteFlow. The software is an extended version of Automatic Conﬁguration of Routing Control Platforms (https://github.com/routeflow/AutomaticConfigurationRouteFlow) and RouteFlow (https://github.com/CPqD/RouteFlow/).   

This code is contributed by Mengchen Shi - TUB under supervision of Apoorv Shukla. For any question or bug, please report at mcshi.tommy@gmail.com and apoorv@inet.tu-berlin.de.  

Version : 3.0  

### What is this repository for? ###

* For automatically generating custom topologies in SDN
* For teaching, research, or other purpose use Routing protocols in OPENFLOW environment
* Create RouteFlow automatically upon discovery any OpenFlow network
### How to start ###
* Start the rfauto

		cd acrf-extended/rftest

		sudo ./rfauto

(if you want use the version with FlowVisor and Discovery Controller(RYU), use "sudo ./rfauto -f")

* Start a Mininet script with the IP and port of the EACRF

Port number: 

 With FlowVisor: 6600

 Without FlowVisor: 6633


### Contributions ###
* Contains topology generator tool (Look in Source for the directory: superrace-topology-generator-tui-3bf8fb04f472) which automatically generates the topologies in SDN. Topology generator tool can be used independently or in conjunction with automatic configuration for configuring protocols like BGP, OSPF. You can go to the directory in Source: superrace-topology-generator-tui-3bf8fb04f472 and follow the instructions in the corresponding README.
* Added Full BGP Supported, multiple AS supported.
* Decouple the functions of discovery message(xmlagent.py) and VM generation(vmserver.py) so can fit for other RouteFlow Version and more controllers.
* Enhanced stability and fewer bugs for VM Generation.
* Anyone can modify the config files easily.
* Make automatic generation of topologies and configuration of routing protocols convenient in SDN.

### Requirement ###
System: Ubuntu 12.04

Others:
sudo apt-get install build-essential git libboost-dev libboost-program-options-dev libboost-thread-dev libboost-filesystem-dev iproute-dev openvswitch-switch python-pymongo lxc ant openjdk-6-jdk python-pexpect python-ipaddr python-dev mongodb python-pip

MongoDB: can be updated to newest version 2.4.14(See documentation how to install specific version)

### What can this repository do?###


### How do I get set up? ###

* Install the dependencies
* Clone the below repository
* git clone git@bitbucket.org:Apoorv1986/automatic-custom-topology-generation-and-configuration.git
* cd acrf-extended
* make rfclient
* cd FLOWVISOR
* make
* pip install --upgrade pip
* pip install ryu

### Note ###
1. For some case, reverse path will happend and icmp will be blocked by default firewall rule of unix. So use this command to disable rp_filter in all ports involved in experiment.
sudo sysctl -w "net.ipv4.conf.all.rp_filter=0"
sudo sysctl -w "net.ipv4.conf.default.rp_filter=0"
2. Without flowvisor, use port 6633 as controller port and use sudo ./rfauto to start
2. with FlowVisor, use port 6600 as controller port and use "sudo ./rfauto -f"