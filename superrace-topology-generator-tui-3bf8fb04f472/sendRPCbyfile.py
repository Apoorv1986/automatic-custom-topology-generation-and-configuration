from rpcagent import Agent
import os
def intid_to_dpid12(intid):
    if intid < 16:
        dpid12 = '00-00-00-00-00-0' + hex(intid)[2:]
    else :
        dpid12 = '00-00-00-00-00-' + hex(intid)[2:]
    return dpid12

with open("temp_switches.txt") as f:	
    for line in f:
        (dpid, ports) = line.split()
        dpid_hex=intid_to_dpid12(int(dpid))
        Agent.join_switch(dpid_hex,ports)

with open("temp_links.txt") as f:
    for line in f:
        (dpid1, port1, dpid2, port2) = line.split()
        dpid1_hex=intid_to_dpid12(int(dpid1))
        dpid2_hex=intid_to_dpid12(int(dpid2))
        Agent.join_link(dpid1_hex, port1, dpid2_hex, port2) 
os._exit(0)