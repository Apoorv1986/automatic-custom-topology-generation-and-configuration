#!/usr/bin/python

import os
import sys
import readline
import re
import os.path
import networkx as nx
import numpy as np
import subprocess 

type_num = 0
type_name = ''
output_file_name = ''
input_file_name = ''
filename = ''
controller_ip = ''
num_switches = 2
depth_tree = 1
random_prob = 0.3
random_edge = 1
mesh_line = 1
mesh_row = 1
default_controller = '127.0.0.1'
controller_port = ''
default_port = '6633'
default_outfile = 'custom'
AS={}
labels = {}
command=''

def sanitised_input(prompt, type_=None, min_=None, max_=None, range_=None): 
    if min_ is not None and max_ is not None and max_ < min_: 
        raise ValueError("min_ must be less than or equal to max_.") 
    while True: 
        ui = raw_input(prompt) 
        if ui == 'z':
            return 'UNDO'
        if type_ is not None: 
            try: 
                ui = type_(ui) 
            except ValueError: 
                print("    Retry input: Input type must be {0}.".format(type_.__name__)) 
                continue
        if max_ is not None and ui > max_: 
            print("    Retry input: Input must be less than or equal to {0}.".format(max_)) 
        elif min_ is not None and ui < min_: 
            print("    Retry input: Input must be greater than or equal to {0}.".format(min_)) 
        elif range_ is not None and ui not in range_: 
            if isinstance(range_, range): 
                template = "    Retry input: Input must be between {0.start} and {0.stop}."
                print(template.format(range_)) 
            else: 
                template = "    Retry input: Input must be {0}."
                if len(range_) == 1: 
                    print(template.format(*range_)) 
                else: 
                    print(template.format(" or ".join((", ".join(map(str, 
                                                                     range_[:-1])), 
                                                       str(range_[-1]))))) 
        else: 
            return ui 

def re_input(prompt, regex, ErrorMessage):
    while True:
        ui = raw_input(prompt) 
        if ui == 'z':
            return 'UNDO'
        elif re.match(regex, ui):
            return ui
        else :
            print(ErrorMessage)
            continue


def type_input():
    global type_num
    global type_name
    #Must to disable rp_filter
    command = "sudo bash disable_rp_filter"
    os.system(command)
    print ('''\033[32m
     ==========================================
    |     Mininet Topology Generator - TUI     |
    |                   V6.0                   |
    |           mcshi.tommy@gmail.com          |
     ==========================================
    -> *Please select the topology you want to generate
    ->  1.Fullmesh
    ->  2.Ring
    ->  3.Star
    ->  4.Linear
    ->  5.Binary Tree
    ->  6.2-D Mesh
    ->  7.Random - Erdos renyi Graph
    ->  8.Random - Random edge graph
    ->  9.Topology Zoo\033[0m''')
    type_num = sanitised_input("\033[32m    Enter Number of type: \033[0m", int, 1, 9)
    if type_num == 1:
        type_name = 'fullmesh'
    elif type_num == 2:
        type_name = 'ring'
    elif type_num == 3: 
        type_name = 'star'
    elif type_num == 4:
        type_name = 'linear'
    elif type_num == 5:
        type_name = 'binarytree'
    elif type_num == 6:
        type_name = '2d_mesh'       
    elif type_num == 7:
        type_name = 'random_erdos_renyi'
    elif type_num == 8:
        type_name = 'random_edge'        
    elif type_num == 9:
        type_name = 'topozoo'
    return type_num

def controller_ip_input():
    global controller_ip
    print "\033[32m    -> You can type 'z' in following steps to go back except \033[31mRed Questions! \033[0m"
    controller_ip = re_input("\033[32m    -> Please type the ip address of the controller\n    Type 'ENTER' use default setting ({}) :\033[0m".format(default_controller),
                             "(\d+)\.(\d+)\.(\d+)\.(\d+)|^\s*$",
                             "    Retry input: Input Should only contain digitals and '.' or leave it blank")
    if controller_ip == '':
        controller_ip = default_controller
    return controller_ip

def controller_port_input():
    global controller_port
    controller_port = re_input("\033[32m    -> Please type the port of the controller\n    Type 'ENTER' use default setting (6633) :\033[0m",
                            "^[\d\s]*$",
                            "    Retry input: Input Should only contain digitals or leave it blank")
    if controller_port == '':
        controller_port = default_port  
    return controller_port

def output_file_name_input():
    global output_file_name,filename
    output_file_name = re_input("\033[32m    -> Please type name of the output file[without'.py']\n    Type 'ENTER' use default setting (custom.py) \033[0m",
                                "^[\w\s]*$",
                                "    Retry input: Input Should only contain alphabets and digitals")
    if output_file_name == '':
        output_file_name = default_outfile
    filename = '%s.py' %output_file_name
    return output_file_name

def mininet_generate():
    global type_num,num_switches,depth_tree,command,filename
    if type_num < 5 :
        num_switches = sanitised_input("\033[32m    -> *Please type the number of the switches:\033[0m", int, 1, 100)
        if num_switches != 'UNDO':
            command = 'sudo python MininetParser.py -t ' + type_name +' -n ' + str(num_switches) + ' -o custom/' + output_file_name + '.py -c ' + controller_ip+ ' -p ' + controller_port
            os.system(command)
            print "\033[32m    ->  Number of switches is  \033[31m%s \033[0m" %num_switches

    if type_name == 'binarytree': 
        depth_tree = sanitised_input("\033[32m    -> *Please type the depth of the binary tree topology[1-10]:\033[0m", int, 1, 10)
        if depth_tree == 'UNDO':
            num_switches = depth_tree
        else : 
            num_switches = 2 ** int(depth_tree) - 1
            command = 'sudo python MininetParser.py -t ' + type_name +' -d ' + str(depth_tree) + ' -o custom/' + output_file_name + '.py -c ' + controller_ip+ ' -p ' + controller_port
            os.system(command)
            print "\033[32m    ->  Number of switches is  \033[31m%s \033[0m" %num_switches

    if type_name == '2d_mesh':
        mesh_line = sanitised_input("\033[32m    -> *Input the number on the line of the 2-D Mesh topology[1-10]:\033[0m", int, 1, 10)        
        if mesh_line == 'UNDO':
            num_switches = mesh_line
        else : 
            mesh_row = sanitised_input("\033[31m    -> *Input the number on the row of the 2-D Mesh topology[1-10]:\033[0m", int, 1, 10)
            num_switches = mesh_line * mesh_row
            command = 'sudo python MininetParser.py -t 2d_mesh -ml ' + str(mesh_line) + ' -mr ' + str(mesh_row) + ' -o custom/' + output_file_name + '.py -c ' + controller_ip+ ' -p ' + controller_port
            os.system(command)
            print "\033[32m    ->  Number of switches is  \033[31m%s \033[0m" %num_switches

    if type_name == 'random_erdos_renyi': 
        num_switches = sanitised_input("\033[32m    -> *Please type the number of the switches[1-100]:\033[0m", int, 1, 100)
        if num_switches != 'UNDO':
            random_prob = sanitised_input("\033[31m    -> *Please type the Probability of edge connection[0-1]:\033[0m", float, 0, 1)
            command = 'sudo python MininetParser.py -t ' + type_name +' -n ' + str(num_switches) + ' -np ' + str(random_prob) + ' -o custom/' + output_file_name + '.py -c ' + controller_ip+ ' -p ' + controller_port
            os.system(command)
            print "\033[32m    ->  Number of switches is  \033[31m%s \033[0m" %num_switches

    if type_name == 'random_edge': 
        num_switches = sanitised_input("\033[32m    -> *Please type the number of the switches[1-100]:\033[0m", int, 1, 100)
        if num_switches != 'UNDO':
            random_edge = sanitised_input("\033[31m    -> *Please type the number of edge connection[1-10000]:\033[0m", int, 1, 10000)
            command = 'sudo python MininetParser.py -t ' + type_name +' -n ' + str(num_switches) + ' -ne ' + str(random_edge) + ' -o custom/' + output_file_name + '.py -c ' + controller_ip+ ' -p ' + controller_port
            os.system(command)
            print "\033[32m    ->  Number of switches is  \033[31m%s \033[0m" %num_switches

    # Topology zoo generation
    if type_name == 'topozoo':
        #check if name exists
        from urllib2 import Request, urlopen, HTTPError, URLError
        import urllib
        user_agent = 'Mozilla/20.0.1 (compatible; MSIE 5.5; Windows NT)'
        headers = { 'User-Agent':user_agent }
        while True:
            try:
                input_file_name = re_input("\033[32m    -> *Please type the name of GML file in topology zoo\n    For example: Arpanet196912 :\033[0m",
                                            "^[\w\s]*$",
                                            "    Retry input: Input Should only contain alphabets and digitals") 
                if input_file_name == 'UNDO':
                    num_switches = input_file_name
                    break
                link = "http://www.topology-zoo.org/files/%s.graphml" %(input_file_name)
                name = '%s.graphml' %(input_file_name)
                path = 'zoogml/%s' %(name)
                if os.path.isfile(path):
                    print "\033[32m    ->  %s exists in /zoogml!\033[0m" %name
                    break
                else:
                    req = Request(link, headers = headers)
                    page_open = urlopen(req)
            except HTTPError, e:
                print str(e.code) + ' error, please type a name in database of Topology ZOO'
                continue
            except URLError, e:
                print str(e.reason) + ' error, please type a name in database of Topology ZOO'
                continue
            else:
                print "\033[32m    ->  Start to download %s to ./zoogml/ \033[0m" %name
                urllib.urlretrieve (link, path)
                break
        if input_file_name != 'UNDO':
            g = nx.read_graphml(path, str)
            num_switches = len(g.nodes())
            command = 'sudo python MininetParser.py -t topozoo -f %s -c %s -p %s -o custom/%s.py' %(input_file_name, controller_ip, controller_port, input_file_name)
            os.system(command)
            filename = '%s.py' %input_file_name
            print "\033[32m    ->  Topology custom/%s is ready!\033[0m" % filename
            print "\033[32m    ->  Number of switches is  \033[31m%s \033[0m" % num_switches
    return num_switches

def custom_label():
    global filename
    with open("temp_namefile.txt", 'w') as f:
        f.write('')
    if type_num != 8:
        label = re_input("\033[32m    -> Do you want to customize the name of Switches as Label [y/n] \033[0m",
                    "^[\w\s]*$","Retry input: Input Should only contain alphabets and digitals")
        if label == 'UNDO':
            return label
        else:
            if label == 'y':
                os.system('python networkplot.py &')
                for a in range(1, num_switches+1):
                    key = 's%d' %a
                    labels[key] = re_input("\033[31m    ->  Change the name of Router {} [Default: Router{}]: \033[0m".format(a,a),
                                            "^[\w\s]*$",
                                            "Retry input: Input Should only contain alphabets and digitals")
                    if labels[key] == '':
                        labels[key] = 'Router{}'.format(a)
                #print labels
                with open("temp_namefile.txt", 'w') as f:
                    for k,v in labels.items():
                        line = '{} {}\n'.format(k,v)
                        f.write(line)
                os.system("pkill -f networkplot.py")
                os.system('python networkplot.py &')
            print "\033[32m    ->  Topology custom/%s is ready!\033[0m" %filename

customAS = ''
def if_custom_AS():
    global customAS
    customAS= re_input("\033[32m    -> Do you want to customize Routing configuration [y/n] \033[0m",
                                "^[\w\s]*$",
                                "    Retry input: Input Should only contain alphabets and digitals")
    return customAS
    
protocolnumber = 0
def custom_protocol():
    global protocolnumber
    if customAS == 'y' :
        protocolnumber = sanitised_input('''\033[32m
    -> Please select type of the protocals you want to use
    ->  1.OSPF only
    ->  2.OSPF + iBGP(Single AS for all switches) 
    ->  3.OSPF + eBGP(every switche have its own AS number)
    ->  4.OSPF + mix BGP enviroment(custom BGP AS numbers and switches)
    # Number:\033[0m''', int, 1, 4)
        return protocolnumber

def custom_AS():
    if customAS == 'y' :
            #config discovery controller config rules
        config= '''RPCSERVER,http,127.0.0.1,8000,RPCClient
IPADDRESS_RANGE,172.0.10.1,255.255.255.252,172.100.10.2,255.255.255.252
CONT_IP_ADDRESS,192.169.1.101,255.255.255.0,192.169.1.255,255.255.255.0 
'''
        with open("CONFFILE", 'w') as f:
            f.write(config)
        if protocolnumber == 1:
            config = 'PROTOCOL,OSPF,ZEBRA,OSPF,OSPF\n'
            for i in range(1,num_switches+1) :
                AS[i] = 1
        elif protocolnumber == 2:
            config = 'PROTOCOL,OSPF,ZEBRA,BGP,OSPF\n'
            ibgpas = sanitised_input("\033[31m    -> Please enter AS number[1-65534] :\033[0m", int, 1, 65535)
            for i in range(1,num_switches+1) :
                AS[i] = ibgpas
        elif protocolnumber == 3: 
            config = 'PROTOCOL,OSPF,ZEBRA,BGP,OSPF\n'
            print ("\033[32m    -> In full eBGP, all AS number = 65000 + switch number!\033[0m")
            for i in range(1,num_switches+1) :
                AS[i] = 65000+i
        elif protocolnumber == 4:
            config = 'PROTOCOL,OSPF,ZEBRA,BGP,OSPF\n'
            a = num_switches
            tmp = os.popen("ps -Af").read()
            if 'networkplot.py' not in tmp[:]:  
                os.system('python networkplot.py &')
            while a > 0:
                print '\033[31m    ->  %d switches need to be assigned, %d switches in Total\033[0m' %(a,num_switches)
                ASnumber = sanitised_input('\033[31m    ->  Enter an AS number:\033[0m', int, 1, 65535)
                while True:
                    raw = raw_input('\033[31m    ->  Swiches in this AS(For example: 1-4,6-10 ):\033[0m')
                    if re.match("^[\d\-\,]*$", raw):
                        rangeList = [r.split('-') for r in raw.split(',')]
                        selecctedList = reduce(lambda x,y:x+y,[range(int(l[0]),int(l[-1])+1) for l in rangeList])
                        if any(True for entry in selecctedList if entry in AS):
                            print("    Retry input: You have assigned AS to some switches in this range")
                            continue
                        elif any(True for entry in selecctedList if entry not in range(1,num_switches+1)):
                            print("    Retry input: Some Switch is out of range")
                            continue
                        else :
                            for x in selecctedList:
                                AS[x] = ASnumber
                                a -= 1
                            break
                    else :
                        print("    Retry input: Input should contain digital and ',' '-' only!")
                        continue
                for key, value in AS.items():
                        print '    |AS ',  value, ": S", key
        import collections 
        plotcolorstr = "val_map = {"
        plotlegendstr = "ColorLegend = {"
        color = 1
        asDict = collections.defaultdict(list)
        for sw, as_ in AS.items():
            asDict[as_] += [sw]

        for as_, switches in asDict.items():
            for sw in switches:
                plotcolorstr += "'s" + str(sw) + "': " + str(color) + "," 
            plotlegendstr += "'AS" + str(as_)+ "': "  + str(color) + "," 
            color += 1

        os.system("pkill -f networkplot.py") 
        plotcolorstr += "}\n"
        plotlegendstr += "}\n"

        with open("CONFFILE", 'a') as f:
            f.write(config)
        if protocolnumber ==1 :
            config="OSPF_PARAMETERS,172.0.0.0,8,10,40"
        else :
            config="OSPF_PARAMETERS,172.0.0.0,16,10,40"        
        with open("CONFFILE", 'a') as f:
            f.write(config)
        for i in range(1,num_switches+1):
            if i < 16:
                sdpid = '00-00-00-00-00-0' + hex(i)[2:]
            else :
                sdpid = '00-00-00-00-00-' + hex(i)[2:]
            config = "\nSDPID,%s,172.168.%d.1,24,%d" %(sdpid,i,AS[i])
            with open("CONFFILE", 'a') as f:
                f.write(config)
        plotstr = '''values = [val_map.get(node, 0) for node in g.nodes()]
f = plt.figure(1)
ax = f.add_subplot(1,1,1)
cNorm  = colors.Normalize(vmin=0, vmax=max(values))
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=hsv)
for label in ColorLegend:
    ax.plot([0],[0],color=scalarMap.to_rgba(ColorLegend[label]),label=label)
nx.draw(g,pos,node_size = 500,  cmap = hsv, vmin=0, vmax= max(values),node_color=values,with_labels=True,ax=ax)
plt.axis('off')
f.set_facecolor('w')
plt.legend()
plt.show()
'''
        os.system("head -n -2 networkplot.py > NetworkPlotWithAs.py")
        f=open('NetworkPlotWithAs.py','a')
        f.write(plotcolorstr)
        f.write(plotlegendstr)
        f.write(plotstr)
        f.close()
        if 'random' not in type_name:
            # Random Graph will be different everytime so don't plot new one here
            os.system('python NetworkPlotWithAs.py &')
            # don't use plot inside Mininet scrpit 
            os.system("sed -i -e 's/plt.show/#plt.show/g' custom/{}".format(filename))
        username = raw_input("\033[31m    -> Please enter the user name of the server :\033[0m")
        print "\033[31m    ->  SCP CONFFILE to the server........enter password if ask\033[0m" 
        command = 'scp CONFFILE ' + username + '@' + controller_ip + ':/home/' + username + '/acrf-extended/ryu_app/'
        os.system(command)

def start_mininet():
    print "\033[31m    ->  Start to Clean Mininet\033[0m" 
    os.system('sudo mn -c > /dev/null 2>&1')
    os.system("pkill -f networkplot.py")
    mininetcommand = 'sudo python custom/%s' % filename
    print "\033[31m    ->  Start Mininet using scrpit {}\033[0m".format(filename) 
    #subprocess.Popen(['xterm', '-e', mininetcommand])
    #os.system(mininetcommand)    

type_input()

if type_name == 'topozoo':
    processing_functions = [type_input, controller_ip_input, controller_port_input,mininet_generate,if_custom_AS,custom_protocol,custom_AS,start_mininet]
elif type_name == 'random_erdos_renyi' or type_name == 'random_edge':
    processing_functions = [type_input, controller_ip_input, controller_port_input,output_file_name_input,mininet_generate,start_mininet,if_custom_AS,custom_protocol,custom_AS,]
else :
    processing_functions = [type_input, controller_ip_input, controller_port_input,output_file_name_input,mininet_generate,custom_label,if_custom_AS,custom_protocol,custom_AS,start_mininet]

progress = 1
#function chain, start from controller_ip_input
while progress < len(processing_functions):
    if processing_functions[progress]() == "UNDO":
        progress -= 1
    else:
        progress += 1

print "\033[31m    ->  End of the Topology Generation \033[0m"