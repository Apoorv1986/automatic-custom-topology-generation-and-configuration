#!/usr/bin/python

import sys
import math
import re
import os
from sys import argv

sw_number = ''
output_file_name = 'ipconf'
controller_ip = '192.168.174.140'
controller_port = '6600'
miniedit = False
printargu = False

for i in range(len(argv)):
    if argv[i] == '-n':
        sw_number = argv[i+1]
    if argv[i] == '-o':
        output_file_name = argv[i+1]
    if argv[i] == '-c':
        controller_ip = argv[i+1]
    if argv[i] == '-p':
        controller_port = argv[i+1]
    if argv[i] == '-e':
        miniedit = True
    if argv[i] == '-t':
        printargu = True


# terminate when inputfile is missing
if sw_number  == '':
    sys.exit('\n\t\033[32m    ->    No input switch numbers, use -n argu  to specific the number\033[0m')

# write controller c0 configure: i.e. 'py net.addController('c0', ip="192.168.174.140", port=6600)'
temp1 =''
if miniedit == True:
    temp1 = "py net.addController('c0', ip='{}',port='{}'".format(controller_ip,controller_port)

#WHERE TO PUT RESULTS
outputstring_to_be_exported = ''
outputstring_to_be_exported += temp1
tempstring2 = ''

for i in range(1, int(sw_number)+1):
    #create switch with controller configuration: i.e. 'py net.get('s1').start([c0])'
    temp2 =''
    if miniedit == True:
        temp2 =  "py net.get('s{}').start([c0]) \n".format(str(i))
    #create host ip address configuration: i.e. 'py net.get('h1').cmd('ifconfig h1-eth0 172.168.1.100 netmask 255.255.255.0')'
    temp2 += "py net.get('h{}').cmd('ifconfig h{}-eth0 172.168.{}.100 netmask 255.255.255.0') \n".format(str(i),str(i),str(i))
    #create host ip address configuration: i.e. 'py net.get('h1').cmd('route add default gw 172.168.1.1')'
    temp2 += "py net.get('h{}').cmd('route add default gw 172.168.{}.1') \n\n".format(str(i),str(i))
    tempstring2 += temp2

outputstring_to_be_exported += tempstring2

outputfile = open(output_file_name, 'w')
outputfile.write(outputstring_to_be_exported)
outputfile.close()

print "\033[32m   ->    IP configuration SUCCESSFUL!\033[0m"
print "\033[32m   ->    Printing %s file\033[0m" %output_file_name
command =''
if printargu == True:
    command = "cat "
    command += output_file_name
os.system(command)
print "\033[32m   ->    Please type 'source %s' in Mininet CLI to assign IP address to hosts\033[0m" %output_file_name