from rpcagent import Agent
import os

with open("temp_switches.txt") as f:
    for line in f:
        (dpid, ports) = line.split()
        Agent.join_switch(dpid,ports)

with open("temp_links.txt") as f:
    for line in f:
        (dpid1, port1, dpid2, port2) = line.split()
        Agent.join_link(dpid1, port1, dpid2, port2) 
os._exit(0)