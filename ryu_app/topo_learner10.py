# Copyright (C) 2011 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
An OpenFlow 1.0 L2 learning switch implementation.
"""


from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_0
from ryu.lib.mac import haddr_to_bin
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types
from ryu.topology import event, switches
from ryu.topology.api import get_switch, get_link
from rpcagent import Agent
import copy
import os
def intid_to_dpid12(intid):
    if intid < 16:
        dpid12 = '00-00-00-00-00-0' + hex(intid)[2:]
    else :
        dpid12 = '00-00-00-00-00-' + hex(intid)[2:]
    return dpid12

class SimpleSwitch(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_0.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(SimpleSwitch, self).__init__(*args, **kwargs)
        self.mac_to_port = {}
        self.topology_api_app = self
        self.switches = {}
        self.links = []
        self.rpcsend = {}

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def get_switches_data(self,ev):
        switch_list = get_switch(self.topology_api_app, None)
        for switch in switch_list:
            #print switch.dp.id,len(switch.ports) 
            nodes_dpid = intid_to_dpid12(switch.dp.id) 
            # padded_id = hexid.zfill(12)
            # nodes_dpid = '-'.join(padded_id[i:i+2] for i in range(0, len(padded_id), 2))
            self.switches[nodes_dpid] = len(switch.ports) 
            #print self.switches   
        print "Find switches:", len(self.switches) 
        links_list = get_link(self.topology_api_app, None)   
        self.links=[(link.src.dpid,link.src.port_no,link.dst.dpid,link.dst.port_no) for link in links_list] 
        #self.links=['{}.{}.{}.{}'.format(link.src.dpid,link.src.port_no,link.dst.dpid,link.dst.port_no) for link in links_list] 
        #print self.links
        print "Find links:",(len(self.links)/2)
        with open("temp_switches.txt", 'w') as f:
            f.write('')
        with open("temp_links.txt", 'w') as f:
            f.write('')
        i = 0
        for nodes_dpid in self.switches:
            i += (self.switches[nodes_dpid]- 1)
        if i > len(self.links):
            print "link information missing"
            get_link(self, None)
        elif i < len(self.links):
            print "switch information missing"
            get_switch(self, None)
        elif i == len(self.links) and i != 0:
            print "Discovery Finish, start to send msg to VMserver"
            print "ALL links",(len(self.links)/2)
            print "ALL switches", len(self.switches)

            with open("temp_switches.txt", 'w') as f:
                for dpid in self.switches:
                    if dpid not in self.rpcsend:
                        #print dpid, (self.switches[dpid]+1)
                    
                        line = '{} {}\n'.format(dpid,(self.switches[dpid]+1))
                        f.write(line)
                        Agent.join_switch(dpid,(self.switches[dpid]+1))
                    self.rpcsend[dpid] = 1
            with open("temp_links.txt", 'w') as f:
                for alink in self.links:
                    print alink
                    link1 = '{}-{}'.format(alink[0],alink[2])

                    link2 = '{}-{}'.format(alink[2],alink[0])

                    if link1 not in self.rpcsend or link2 not in self.rpcsend:
                        #print intid_to_dpid12(alink[0]),str(alink[1]),intid_to_dpid12(alink[2]),str(alink[3])
                        Agent.join_link(intid_to_dpid12(alink[0]),str(alink[1]),intid_to_dpid12(alink[2]),str(alink[3]))
                        line = '{} {} {} {}\n'.format(intid_to_dpid12(alink[0]),str(alink[1]),intid_to_dpid12(alink[2]),str(alink[3]))
                        f.write(line)
                        self.rpcsend[link1] = 1
                        self.rpcsend[link2] = 1
            print "\n\n\n\n\n\n\n\nDiscovery was done, quit discovery controller"
            os._exit(0)

