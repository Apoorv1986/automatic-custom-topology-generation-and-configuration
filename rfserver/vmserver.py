#!/usr/bin/env python2.7
# -*- coding:utf-8 -*-

import sys
import os
import logging
import threading
import time
import argparse
import ipaddr
import shlex
import subprocess

import rflib.ipc.IPC as IPC
import rflib.ipc.MongoIPC as MongoIPC

from rflib.ipc.RFProtocolFactory import RFProtocolFactory
from rflib.ipc.RFProtocol import *
from rflib.defs import *
from rflib.types.Match import *
from rflib.types.Action import *
from rflib.types.Option import *
from rftable import *
from SimpleXMLRPCServer import SimpleXMLRPCServer
from rfserver import RFServer

# Register actions
REGISTER_IDLE = 0
REGISTER_ASSOCIATED = 1
REGISTER_ISL = 2
LXCDIR = "/var/lib/lxc"

# server = SimpleXMLRPCServer(("localhost", 8000), logRequests=True, allow_none=True)

line = "\nlxc.arch = amd64\nlxc.cap.drop = sys_module mac_admin\nlxc.pivotdir = lxc_putold\n\n# uncomment the next line to run the container unconfined:\n#lxc.aa_profile = unconfined\n\nlxc.cgroup.devices.deny = a\n# Allow any mknod (but not using the node)\nlxc.cgroup.devices.allow = c *:* m\nlxc.cgroup.devices.allow = b *:* m\n# /dev/null and zero\nlxc.cgroup.devices.allow = c 1:3 rwm\nlxc.cgroup.devices.allow = c 1:5 rwm\n# consoles\nlxc.cgroup.devices.allow = c 5:1 rwm\nlxc.cgroup.devices.allow = c 5:0 rwm\n#lxc.cgroup.devices.allow = c 4:0 rwm\n#lxc.cgroup.devices.allow = c 4:1 rwm\n# /dev/{,u}random\nlxc.cgroup.devices.allow = c 1:9 rwm\nlxc.cgroup.devices.allow = c 1:8 rwm\nlxc.cgroup.devices.allow = c 136:* rwm\nlxc.cgroup.devices.allow = c 5:2 rwm\n# rtc\nlxc.cgroup.devices.allow = c 254:0 rwm\n#fuse\nlxc.cgroup.devices.allow = c 10:229 rwm\n#tun\nlxc.cgroup.devices.allow = c 10:200 rwm\n#full\nlxc.cgroup.devices.allow = c 1:7 rwm\n#hpet\nlxc.cgroup.devices.allow = c 10:228 rwm\n#kvm\nlxc.cgroup.devices.allow = c 10:232 rwm\n"

#start = time.time()

def str_to_dpid(s):
    """
    Convert a DPID in the canonical string form into a long int.
    """
    if s.lower().startswith("0x"):
        s = s[2:]
    s = s.replace("-", "").split("|", 2)
    a = int(s[0], 16)
    if a > 0xffFFffFFffFF:
        b = a >> 48
        a &= 0xffFFffFFffFF
    else:
        b = 0
    if len(s) == 2:
        b = int(s[1])
    return str(a | (b << 48))

def ssh_flow_check(n):
    import paramiko
    ssh=paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect('mininet',username='mininet',password='mininet')
    stdin,stdout,stderr = ssh.exec_command("sudo python -u ~/topology-generator/flowcheck.py -m {} &".format(n),bufsize=1)

    while True:
        out = stdout.read(1)
        if out == '':
            break
        if out != '':
            sys.stdout.write(out)
            sys.stdout.flush()

    ssh.close()
    return 'DONE'

def local_flow_check(n):
    import commands
    from sys import argv
    max = n
    start = time.time()
    print "->   Wait for initial time {} seconds.".format(max*2)
    time.sleep(max*2)
    print "->   Start to check reachability."
    r = 0
    while True:
        i = 0
        r += 1  
        for x in range(1, max+1):
            for y in range(1, max+1):
                if y !=x:
                    command = "sudo ovs-ofctl dump-flows s{} | grep 172.168.{}.0".format(x,y)
                    #print command
                    if len(commands.getoutput(command)) != 0:
                        i += 1
                        #print '{} have  {}  in flowtable!'.format(x,y)
                        #print len(commands.getoutput(command)) 
                    else:
                        if r > (max):
                        #   print "RESTART {}".format(x)
                        # if (time.time()-start) > max*6:
                            print 'S{} have no 172.168.{}.0/24 in flowtable'.format(x,y)
                    #   # only exit when all output != 0 and give a time 
        left = ((max * (max-1))-i)
        print "->   Checking flowtables................{} flows missing.".format(left)
        if left > 0: 
            time.sleep(10)
        # print str(i) + ' flows'
        if i == max * (max-1):  
            print "->   All switches should reachable now. Try 'pingall' in mininet."
            break
        if (time.time()-start) > 300:
            print "->   5 Minutes passed, stop checking flow tables. Please check reachability and debug the system."
            break
    print 'done'
    return 'DONE'

class VMServer(RFServer, object):

    def __init__(self, configfile, islconffile):
        super(VMServer, self).__init__(configfile, islconffile)
        self.ASnum = {}
        self.switch = {}
        self.link = {}
        self.linkname = {}
        self.ent = 0
        self.switchports = {}
        self.switchportcounter = {}
        self.bgp = 1
        self.LRR = ""
        self.configuredlink = {}
        self.switchcounter = 0
        self.nodestimer = 0
        self.linktimer = 0
        self.oftimer = 0
        self.start = 0

        # logging
        with open("rftest/CONF_FILE", 'w') as f:
            f.write("vm_id,vm_port,ct_id,dp_id,dp_port")

    def action_on_link(self, ent):
        entries = self.linkname[ent]
        # Message:"00-00-00-00-00-02,2,172.0.15.1,255.255.255.252,172.0.15.0,172.0.15.3,00-00-00-00-00-01,2,172.0.15.2,"     
        if len(self.configuredlink) == 0:
            self.nodestimer = time.time()
            print "\033[31m->  Stage 1 finished: it took {} seconds to discovery all the nodes for {} switches.\033[0m".format((self.nodestimer-self.start), len(self.switch))
        if self.linkname[ent] == 1:
            return
        idlocal = str_to_dpid(entries[1])
        localportnum = entries[2]
        iplocal = entries[3]
        mask = entries[4]
        subnet = entries[5]
        idremote = str_to_dpid(entries[7])
        remoteportnum = entries[8]
        ipremote = entries[9]
        configuredlink1 = "{}-{}-{}-{}".format(idlocal, localportnum,idremote, remoteportnum)
        configuredlink2 = "{}-{}-{}-{}".format(idremote, remoteportnum,idlocal, localportnum)
        if configuredlink1 in self.configuredlink or configuredlink2 in self.configuredlink:
            return  
        else: 
            self.configuredlink[configuredlink1] = 0
            self.configuredlink[configuredlink2] = 0

        loopadd1 = idlocal + '.' + idlocal + '.' + idlocal + '.' + idlocal
        loopadd2 = idremote + '.' + idremote + '.' + idremote + '.' + idremote

        lxcname = "rfvm" + str_to_dpid(entries[1])
        filename = "/var/lib/lxc/{}/rootfs/etc/quagga/zebra.conf".format(lxcname)

        with open(filename, 'a') as zebrafile:
            zebrafile.write("interface eth{}".format(localportnum))
            networkstr = "{}/{}".format(iplocal, mask)
            address = ipaddr.IPv4Network(networkstr)
            masklength = str(address.prefixlen)
            zebrafile.write("\n        ip address " + iplocal + "/" + masklength + "\n!\n")

            # ospf neighbor or static route to remote loopback
            if self.ASnum[idremote] != self.ASnum[idlocal]:
                zebrafile.write("ip route " + loopadd2 + "/32 " + ipremote + "\n!\n")
            else:
                with open("/var/lib/lxc/" + lxcname + "/rootfs/etc/quagga/ospfd.conf", 'a') as ospfile:
                    ospfile.write("        network " + subnet + "/" + masklength + " area 0\n")

        # bgp neighbor
        with open("/var/lib/lxc/"+lxcname+"/rootfs/etc/quagga/bgpd.conf", 'a') as bgpfile:
            if self.ASnum[idremote] != self.ASnum[idlocal]:
                bgpfile.write("        neighbor " + loopadd2 + " remote-as " + self.ASnum[idremote]  + " \n")
                bgpfile.write("        neighbor " + loopadd2 + " ebgp-multihop \n")
                bgpfile.write("        neighbor " + loopadd2 + " update-source " + loopadd1 + " \n")
                # L="        neighbor " + loopadd2 + " next-hop-self \n"
            self.switchportcounter[lxcname] += 1

        # os command
        if self.switchportcounter[lxcname] == self.switchports[lxcname]:
            with open("/var/lib/lxc/"+lxcname+"/rootfs/etc/quagga/bgpd.conf", 'a') as bgpfile:
                i=0
                for idnum in self.ASnum:
                    if self.ASnum[idnum] == self.ASnum[idlocal] and idnum != idlocal:
                        i+=1
                        bgpfile.write("        neighbor " + idnum + "." + idnum + "." + idnum + "." + idnum + " peer-group internal \n")
            #print '\033[31m->  ======> switch {}   have peer with   {} switches in same AS\033[0m'.format(idlocal,i) #debug for lost iBGP configuration
            attach = "sudo lxc-attach -n " + lxcname + " -- "
            command = attach + '/etc/init.d/quagga restart  > /dev/null 2>&1'
            result_code = os.system(command)
            command = attach + '/usr/lib/quagga/zebra -d  > /dev/null 2>&1'
            result_code = os.system(command)
            self.switchcounter += 1
            switchleft = len(self.switch) - self.switchcounter
            print "\033[31m->  Configuration of %s is done! %d switches left!\033[0m" %(lxcname, switchleft)

        # remote peer configure
        lxcname = "rfvm" + idremote
        with open("/var/lib/lxc/"+lxcname+"/rootfs/etc/quagga/zebra.conf", 'a') as zebrafile:
            zebrafile.write("interface eth{}".format(remoteportnum))
            networkstr = "{}/{}".format(ipremote, mask)
            address = ipaddr.IPv4Network(networkstr)
            masklength = str(address.prefixlen)
            zebrafile.write("\n        ip address " + ipremote + "/" + masklength + "\n!\n")

            # ospf neighbor or static route to remote loopback
            if self.ASnum[idremote] != self.ASnum[idlocal]:
                zebrafile.write("ip route " + loopadd1 + "/32 " + iplocal + "\n!\n")
            else:
                with open("/var/lib/lxc/" + lxcname + "/rootfs/etc/quagga/ospfd.conf", 'a') as ospfile:
                    ospfile.write("        network " + subnet + "/" + masklength + " area 0\n")       

        # BGP part
        with open("/var/lib/lxc/"+lxcname+"/rootfs/etc/quagga/bgpd.conf", 'a') as bgpfile:
            if self.ASnum[idremote] != self.ASnum[idlocal]:
                bgpfile.write("        neighbor " + loopadd1 + " remote-as " + self.ASnum[idlocal]  + " \n")
                bgpfile.write("        neighbor " + loopadd1 + " ebgp-multihop \n")
                bgpfile.write("        neighbor " + loopadd1 + " update-source " + loopadd2 + " \n")
                # bgpfile.writ("        neighbor " + loopadd2 + " next-hop-self \n")
            self.switchportcounter[lxcname] += 1
        
        if self.switchportcounter[lxcname] == self.switchports[lxcname]:
            with open("/var/lib/lxc/"+lxcname+"/rootfs/etc/quagga/bgpd.conf", 'a') as bgpfile:
                i = 0
                for idnum in self.ASnum:
                    if self.ASnum[idnum] == self.ASnum[idremote] and idnum != idremote:
                        i+=1
                        bgpfile.write("        neighbor " + idnum + "." + idnum + "." + idnum + "." + idnum + " peer-group internal \n")
            #print '\033[31m->  ======> switch {}   have peer with   {} switches in same AS\033[0m'.format(idremote,i) #debug for lost iBGP configuration
            attach = "sudo lxc-attach -n " + lxcname + " -- "
            command = attach + '/etc/init.d/quagga restart  > /dev/null 2>&1'
            result_code = os.system(command)
            command = attach + '/usr/lib/quagga/zebra -d  > /dev/null 2>&1'
            result_code = os.system(command)
            self.switchcounter += 1
            switchleft = len(self.switch) - self.switchcounter
            print "\033[31m->  Configuration of %s is done! %d switches left!\033[0m" %(lxcname, switchleft)
        self.linkname[ent] = 1
        self.configuredlink[configuredlink1] = 1
        self.configuredlink[configuredlink2] = 1
        if (len(self.switch) - self.switchcounter) == 0: 
            self.linktimer = time.time()
            print "\033[31m->  Stage 2 finished: it took {} seconds for all {} links configuration.\033[0m".format((self.linktimer - self.nodestimer), len(self.configuredlink)/2)
            print "\033[31m->  Installing routing table and flow table. Reachability checking will start.\033[0m"
            if local_flow_check(len(self.switch)) == 'DONE':
            # if ssh_flow_check(len(self.switch)) == 'DONE':
                self.oftimer = time.time()
                print "\033[31m->  Stage 3 finished: it took {} seconds for all switches learn the FlowTable.\033[0m".format((self.oftimer - self.linktimer))
                print "\033[31m->  Stage 1 Creat {} Nodes: {} \033[0m".format( len(self.switch), (self.nodestimer-self.start))
                print "\033[31m->  Stage 2 Creat {} Links: {} \033[0m".format( len(self.configuredlink)/2, (self.linktimer-self.start))
                print "\033[31m->  Stage 3 Install Tables: {} \033[0m".format((self.oftimer-self.start))
                print "\033[31m->  {} {} {} {} {}\033[0m".format(len(self.switch),len(self.configuredlink)/2,(self.nodestimer-self.start),(self.linktimer-self.start),(self.oftimer-self.start))
                # if self.bgp == 1:
                    # count = sum(len(v) for v in d.itervalues())
                #     print "\033[31m->  There is {} AS in BGP.\033[0m".format(sum(self.ASnum.values()))
                print "\033[31m->  All stages are done. Totally takse {} seconds.\033[0m".format((self.oftimer-self.start))
            
    def action_on_switch_join(self, entries):
        lxcid = str_to_dpid(entries[1])
        lxcname = "rfvm"+lxcid
        command = " lxc-clone -o base -n " + lxcname
        print "\033[31m->  Creating %s!\033[0m" % (lxcname)
        length = len(entries)
        result_code = os.system(command)
        # write config file of lxc
        loopadd = lxcid + '.' + lxcid + '.' + lxcid + '.' + lxcid
        filename = "/var/lib/lxc/"+lxcname+"/config"
        f=open(filename,'w')
        L="lxc.utsname = "+lxcname
        f.write(L)
        Net="\nlxc.network.type = veth\nlxc.network.flags = up\n"
        f.write(Net)
        stri=entries[1]
        hw="lxc.network.hwaddr = "+str("aa")+":" +str("aa")+":"+str("aa")+":"+str("aa")+":"+str("aa")+":"+str(stri[15])+str(stri[16])
        f.write(hw)
        f.write("\nlxc.network.name = eth0")
        f.write("\nlxc.network.link = lxcbr0\n")
        #number of ports
        a=int(entries[2])
        #set counter for ports
        self.switchports[lxcname] = a
        self.switchportcounter[lxcname] = 2
        for x in range(1,a):
            f.write(Net)
            L="lxc.network.name = eth" + str(x) + "\n"
            f.write(L)
            L="lxc.network.veth.pair = " + lxcname + "." + str(x) + "\n"
            f.write(L)
            f.write(hw)
            f.write("\n")
        L="lxc.devttydir = lxc\nlxc.tty = 4\nlxc.pts = 1024\n"
        f.write(L)
        L="lxc.rootfs = /var/lib/lxc/"+ lxcname + "/rootfs"
        f.write(L)
        L="\nlxc.mount = /var/lib/lxc/"+ lxcname+ "/fstab"
        f.write(L)
        f.write(line)
        f.close()
        # #write network interface file of lxc
        filename="/var/lib/lxc/"+lxcname+"/rootfs/etc/network/interfaces"
        f=open(filename,'w')
        L="auto lo\niface lo inet loopback\n\nauto eth0\niface eth0 inet static"
        f.write(L)
        L="\n    address " + entries[3]
        f.write(L)
        L="\n    netmask " + entries[4]
        f.write(L)
        L="\n    network " + entries[5]
        f.write(L)
        L="\n    broadcast " + entries[6]
        f.write(L)
        f.write("\n")
        L="auto lo:1\niface lo:1 inet static\n    address " + loopadd + "\n    netmask 255.255.255.255\n"
        f.write(L)
        ## copy rc.local
        command="cp conf/rc.local /var/lib/lxc/"+lxcname+"/rootfs/etc/"
        result_code = os.system(command)
        ## copy sysctl
        command="cp conf/sysctl.conf /var/lib/lxc/"+lxcname+"/rootfs/etc/"
        result_code = os.system(command)
        ## mkdir rfclient
        command="mkdir /var/lib/lxc/"+lxcname+"/rootfs/opt/rfclient"
        result_code = os.system(command)
        ## copy rfclient
        command="cp build/rfclient /var/lib/lxc/"+lxcname+"/rootfs/opt/rfclient/rfclient"
        result_code = os.system(command)
        ## copy runrfclient
        command="cp conf/run_rfclient.sh /var/lib/lxc/"+lxcname+"/rootfs/root/"
        result_code = os.system(command)
        command="chmod +x /var/lib/lxc/"+lxcname+"/rootfs/root/run_rfclient.sh"
        result_code = os.system(command)
        ## code for daemon file in quagga
        protocol={}
        for x in range(7,length):
            if entries[x]=="PRN":
                break
            if entries[x]=="PR":
                continue
            protocol[entries[x]]=1
        filename="/var/lib/lxc/"+lxcname+"/rootfs/etc/quagga/daemons"
        f.close()
        f=open(filename,'w')
        if "ZEBRA" in protocol:
            f.write("zebra=yes\n")
        else:
            f.write("zebra=no\n")
        if "BGP" in protocol:
            f.write("bgpd=yes\n")
        else:
            f.write("bgpd=no\n")
        if "OSPF" in protocol:
            f.write("ospfd=yes\n")
        else:
            f.write("ospfd=no\n")
        if "OSPF6" in protocol:
            f.write("ospf6d=yes\n")
        else:
            f.write("ospf6d=no\n")
        if "RIP" in protocol:
            f.write("ripd=yes\n")
        else:
            f.write("ripd=no\n")
        if "RIPNG" in protocol:
            f.write("ripngd=yes\n")
        else:
            f.write("ripngd=no\n")
        if "ISIS" in protocol:
            f.write("isisd=yes\n")
        else:
            f.write("isisd=no\n")
        if "ISIS" in protocol:
            f.write("babeld=yes\n")
        else:
            f.write("babeld=no\n")
        ## copy debian.conf quagga
        command="cp conf/debian.conf /var/lib/lxc/"+lxcname+"/rootfs/etc/quagga/"
        result_code = os.system(command)
## make ospf.conf
        y=x+1
        network={}
        ipAddress={}
        mask={}
        hello=1
        dead=4       
        while y<length:
            if entries[y]=="\"":
                break
            if entries[y]=="NETWORK":
                y=y+1
                network[entries[y]]=entries[y+1]
            if entries[y]=="HELLO_INTERVAL":
                y=y+1
                hello=entries[y]
            if entries[y]=="ROUTER_DEAD_INTERVAL":
                y=y+1
                dead=entries[y]
            if entries[y]=="NEW":
                y=y+1
                ipAddress[entries[y]]=entries[y+1]
                y=y+1
                mask[entries[y-1]]=entries[y+1]
            if entries[y]=="AS":
                y=y+1
                self.ASnum[lxcid] = entries[y]
            y=y+1
        
        # #OSPF conf 
        filename="/var/lib/lxc/"+lxcname+"/rootfs/etc/quagga/ospfd.conf"
        f.close()
        f=open(filename,'w')
        L="password en\nenable password en\n!\n"
        f.write(L)
        L="log file /var/log/quagga/ospfd.log\n!\n"
        f.write(L)
        for x in range(1,a):
            L="interface eth"+str(x)
            f.write(L);
            L="\n        ip ospf hello-interval " + str(hello) + "\n        ip ospf dead-interval " + str(dead) + "\n!\n"
            f.write(L)
        L="router ospf\n"
        f.write(L)
        # for net in network:
        #    L="        network "+net+"/"+network[net]+" area 0\n"
        #    f.write(L);
        L="        router-id " + loopadd + "\n        network "+ loopadd + "/24 area 0\n"
        f.write(L)
        if "BGP" not in protocol:
            for x in ipAddress:
                L="        network "+ ipAddress[str(x)] + "/" + mask[str(x)] +  " area 0\n"
                f.write(L)
        ##ZEBRA conf    
        filename="/var/lib/lxc/"+lxcname+"/rootfs/etc/quagga/zebra.conf"
        f.close()       
        f=open(filename,'w')
        L="password en\nenable password en\n!\nlog file /var/log/quagga/zebra.log\npassword en\nenable password en\n!\n"
        f.write(L)
        for x in ipAddress:
            L="interface eth"+str(x)
            f.write(L)
            L="\n        ip address "+ ipAddress[str(x)] + "/" + mask[str(x)] +  "\n!\n"
            f.write(L)
        f.close()

        ##BGP conf
        filename="/var/lib/lxc/"+lxcname+"/rootfs/etc/quagga/bgpd.conf"
        f=open(filename,'w')
        L="password en\nenable password en\n!\nrouter bgp "+ self.ASnum[lxcid] +"\n        no synchronization\n        no auto-summary\n"
        f.write(L)
        L="        bgp router-id " +  loopadd + "\n"
        f.write(L)
        if "BGP" in protocol:
            self.bgp = 1
            for x in ipAddress:
                L="        network "+ ipAddress[str(x)] + "/" + mask[str(x)] +  "\n"
                f.write(L)
            L="        neighbor internal peer-group\n"
            f.write(L)
            L="        neighbor internal remote-as "+self.ASnum[lxcid]+"\n"
            f.write(L)
            L="        neighbor internal update-source " + loopadd + "\n"
            f.write(L)
            L="        neighbor internal next-hop-self\n"
            f.write(L)
              
        f.close() 
        #copy passwd files
        command="cp conf/passwd /var/lib/lxc/"+lxcname+"/rootfs/etc/"
        result_code = os.system(command)

        #starting lxc
        command="lxc-stop -n " + lxcname
        result_code = os.system(command)
        command="lxc-start -n " + lxcname + " -d"
        result_code = os.system(command)
        print "\033[31m-> Number of switches have been created : {}.\033[0m".format(len(self.switch)) 
        command="lxc-wait -n "+lxcname+ " -s RUNNING"
        result_code = os.system(command)

        #starting control plane
        filename="rftest/CONF_FILE"
        f=open(filename,'a')
        dp=str(stri[0])+str(stri[1]) +str(stri[3])+str(stri[4])+str(stri[6])+str(stri[7])+str(stri[9])+str(stri[10])+str(stri[12])+str(stri[13])+str(stri[15])+str(stri[16])
        lxc=str("aa")+str("aa")+str("aa")+str("aa")+str("aa")+str(stri[15])+str(stri[16])
        
        for x in range(1,a):
            name=lxcname+ "." +str(x)
            command='ovs-vsctl add-port dp0 '+ lxcname+ "." +str(x)
            print command
            result_code = os.system(command)
            command="\n"+lxc+","+str(x)+",0,"+dp+","+str(x)
            f.write(command)    
        f.close()
        command = "sudo lxc-attach -n " + lxcname + " -- /bin/bash -c 'route add -host " + loopadd + " dev lo'"
        result_code = os.system(command)
        command = "sudo lxc-attach -n " + lxcname + " -- /bin/bash -c 'echo 0 > /proc/sys/net/ipv4/conf/all/rp_filter'"
        result_code = os.system(command)
        command = 'sudo lxc-attach -n ' + lxcname + " -- /bin/bash -c 'echo 0 > /proc/sys/net/ipv4/conf/default/rp_filter'"
        result_code = os.system(command)        
        self.config = RFConfig(filename,0)
        self.switch[entries[1]]=1
        link=self.link
        self.perform_link_action(self.linkname)

    def perform_link_action(self, linkname):
        leng=len(linkname)
        for links in range(0,leng):
            entries=linkname[links]
            if entries == 1:
                return
            if self.switch[entries[1]] == 0 or self.switch[entries[7]] ==0 :
                return
            else:
                self.action_on_link(links)

    def RPCSERVER_PERFORM_ACTION(self, action):
        ##on switch event   
        entries=action.split(",")
        if entries[0]=="\"join" and entries[1] not in self.switch:
            #start Timer here when recive the first switch join message
            self.switch[entries[1]]=0
            if len(self.switch) == 1:
                self.start = time.time()
                print "\033[31m->  Start the timer here! \033[0m"
            self.action_on_switch_join(entries)

        ##on link event 
        if entries[0]=="\"link":
            self.linkname[self.ent]=entries
            if self.switch[entries[1]] == 1 and self.switch[entries[7]] ==1 :
                self.action_on_link(self.ent)
            else:
                self.link[entries[1]]=entries[7]
            self.ent=self.ent+1
            return

if __name__ == "__main__":
    description='RFServer co-ordinates RFClient and RFProxy instances, ' \
                'listens for route updates, and configures flow tables'
    epilog='Report bugs to: https://github.com/CPqD/RouteFlow/issues'

    parser = argparse.ArgumentParser(description=description, epilog=epilog)
    parser.add_argument('configfile',
                        help='VM-VS-DP mapping configuration file')
    parser.add_argument('-i', '--islconfig',
                        help='ISL mapping configuration file')

    parser.add_argument('-a','--ipAddress',
                        help='IP address of the XMLRPC')
    parser.add_argument('-p','--port',
                        help='port of the XMLRPC')
    args = parser.parse_args()
    print args.ipAddress
    print args.port
    print args.configfile
    try:
        # server = SimpleXMLRPCServer(("localhost", 8000), logRequests=True, allow_none=True)
        server = SimpleXMLRPCServer((args.ipAddress, int(args.port)), logRequests=True, allow_none=True)
        server.register_instance(VMServer(args.configfile, args.islconfig))
        server.serve_forever()
    except IOError:
        sys.exit("Error opening file: {}".format(args.configfile))

 