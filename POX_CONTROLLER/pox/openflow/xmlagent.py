import xmlrpclib
import ipaddr



configfile = file("CONFFILE")
controlIP = controlMask = controlNet = controlBroad = [0, 0, 0, 0]
minIPaddress = maxIPaddress = linkSrcIPaddress = linkDstIPaddress = [0, 0, 0, 0]

found=0
controlIndex = 3
protocol = ["OSPF", "OSPF", "OSPF", "OSPF"]

maxIPNum=1
maxConIPNum=1
net=[0,0,0,0]
broad=[0,0,0,0]
network={}
protoI=[0,0,0,0]
masklength=1
configure={}
AS={}
entries = [line.strip("\n").split(",") for line in configfile.readlines()[0:]]
for (a,b,c,d,e) in entries:
    if a == "RPCSERVER":
        found=1
        http=b
        serverip=c
        serverport=d
    if a == "CONT_IP_ADDRESS":
        controlIP=b.split(".")
        controlMask=c.split(".")
        networkstr = str(b) + "/" + str(c);
        address = ipaddr.IPv4Network(networkstr)
        controlNet = str(address.network)
        controlBroad = str(address.broadcast)
        maxConIPNum = int(ipaddr.IPv4Address(unicode(d)))
    if a == "PROTOCOL":
        protocol[0] = b
        protocol[1] = c
        protocol[2] = d
        protocol[3] = e
    if a == "OSPF_PARAMETERS":
        network[b] = c
        protoI[0] = d
        protoI[1] = e
    if a == "IPADDRESS_RANGE":
        minIPaddress = linkSrcIPaddress = b.split(".")
        linkDstIPaddress[0] = linkSrcIPaddress[0]
        linkDstIPaddress[1] = linkSrcIPaddress[1]
        linkDstIPaddress[2] = linkSrcIPaddress[2]
        linkDstIPaddress[3] = str(int(linkSrcIPaddress[3]) + 1)
        minNetMask=c.split(".")
        networkstr = str(b) + "/" + str(c);
        address = ipaddr.IPv4Network(networkstr)
        net = str(address.network)
        masklength = str(address.prefixlen)
        broad = str(address.broadcast)
        maxIPaddress = d.split(".")
        maxNetMask = e.split(".")
        maxIPNum = int(ipaddr.IPv4Address(unicode(d)))
    if a=="SDPID":
        if b in configure:
            nam=configure[b]
            configure[b]=nam+ ",NEW" + ",1," + c + "," + d 
            nam=AS[b]
            AS[b] = ",AS,"+ e
        else:
            configure[b]= ",NEW" + ",1," + c + "," + d 
            AS[b] = ",AS,"+ e
    if found==0:
        print "RPC Server address is not found"
   
address=http+"://"+serverip+":"+serverport
print address  
print configure.keys()
#XMLRproxy = xmlrpclib.ServerProxy(address)     
#XMLRproxy = xmlrpclib.ServerProxy('http://localhost:8000')   

class XMLAgent(object):
    """docstring for  XMLAgent"""
    
    def __init__(self, address):
        self.proxy = xmlrpclib.ServerProxy(address)
        self.switch = {}
        self.link= {}

    def join_switch(self, datapathidd,ports):
        #add redundancy control: only send xmlagent once.
        if datapathidd in self.switch:
            return
        else: 
            self.switch[datapathidd] = 1

        ipNum=int(ipaddr.IPv4Address(unicode(controlIP[0]+"."+controlIP[1]+"."+controlIP[2]+"."+controlIP[3])))
        if(maxConIPNum - ipNum > 0):
            join = "\"join,"+datapathidd+","+str(ports)+","+str(controlIP[0])+"." +str(controlIP[1])+"." +str(controlIP[2])+"." +str(controlIP[3])
            join += ","+str(controlMask[0])+"." +str(controlMask[1])+"." +str(controlMask[2])+"." +str(controlMask[3])
            join += ","+str(controlNet) + ","+str(controlBroad) 
            join += ",PR,"+protocol[0]+",PR,"+protocol[1] + ",PR,"+protocol[2] + ",PRN" 
            #this can be extended further as more than two protocol can be used in a single vm
            for netw in network:
                join  +=  ",NETWORK,"+netw+","+ network[netw]
                join  +=  ",HELLO_INTERVAL,"+protoI[0]+",ROUTER_DEAD_INTERVAL,"+ protoI[1]
                if datapathidd in configure:
                    join = join +configure[datapathidd]+AS[datapathidd]

            join= join + ",\""
            print join 
            #XMLRproxy.RPCSERVER_PERFORM_ACTION(join)
            self.proxy.RPCSERVER_PERFORM_ACTION(join)

            controlIP[3]= str(int(controlIP[3]) +1)
            if(int(controlIP[3]) >=255):
                controlIP[2] = str(int(controlIP[2]) +1)
                controlIP[3] = str(1)
                if(int(controlIP[2]) >= 255):
                    controlIP[1] =  str(int(controlIP[1]) +1)
                    controlIP[2] = str(1)
                    controlIP[3] = str(1)
                    if(int(controlIP[1]) >=255):
                        controlIP[0] =  str(int(controlIP[0]) +1)
                        controlIP[1] =  str(1)
                        controlIP[2] =  str(1)
                        controlIP[3] =  str(1)
                        if(int(controlIP[0]) >=255):
                            log.info('All the range of control IP address is finished; We cannot configure any other VM now')

        else:
            log.info('Switch Join Event received: all the range of control IP addresses is finished; we cannot configure any other VM now')
                

        
        pass

    def join_link(self, dpid1, port1, dpid2, port2):
        link1 = "{}-{}-{}-{}".format(dpid1, port1, dpid2, port2)
        link2 = "{}-{}-{}-{}".format(dpid2, port2, dpid1, port1)
        if link1 in self.link or link2 in self.link:
            return  
        else: 
            self.link[link1] = 1
            self.link[link2] = 1

        ipNum=int(ipaddr.IPv4Address(unicode(linkDstIPaddress[0]+"."+linkDstIPaddress[1]+"."+linkDstIPaddress[2]+"."+linkDstIPaddress[3])))
        if(maxIPNum - ipNum > 0):
            pstr="\"link,"+dpid1+","+str(port1)+","+linkSrcIPaddress[0]+"."+linkSrcIPaddress[1]+"."+linkSrcIPaddress[2]+"."+linkSrcIPaddress[3] 
            pstr=pstr+ ","+str(minNetMask[0])+"." +str(minNetMask[1])+"." +str(minNetMask[2])+"." +str(minNetMask[3])
            networkstr = linkSrcIPaddress[0]+"."+linkSrcIPaddress[1]+"."+linkSrcIPaddress[2]+"."+ linkSrcIPaddress[3] + "/" + str(minNetMask[0])+"." +str(minNetMask[1])+"." +str(minNetMask[2])+"." +str(minNetMask[3])
            address = ipaddr.IPv4Network(networkstr)

            pstr += ","+str(address.network) + ","+str(address.broadcast)
            pstr += ","+dpid2+","+str(port2)+","+linkDstIPaddress[0]+"."+linkDstIPaddress[1]+"."+ linkDstIPaddress[2]+"."+linkDstIPaddress[3] 
            pstr +=  ",\""
            print pstr
            self.proxy.RPCSERVER_PERFORM_ACTION(pstr)
            if int(linkSrcIPaddress[2])<=255:
                linkSrcIPaddress[2]=str(int(linkSrcIPaddress[2])+1)
                linkDstIPaddress[2]=linkSrcIPaddress[2]
            else:
                if int(linkSrcIPaddress[1])<=255:
                    linkSrcIPaddress[1]=str(int(linkSrcIPaddress[1])+1)
                    linkDstIPaddress[1]=linkSrcIPaddress[1]
                else:
                    if int(linkSrcIPaddress[0])<=255:
                        linkSrcIPaddress[0]=str(int(linkSrcIPaddress[0])+1)
                        linkDstIPaddress[0]=linkSrcIPaddress[0]
                    else:
                        print "Ops, no ip address has left"
        else:
            print "Ops, no ip address has left"


Agent = XMLAgent(address)

if __name__ == '__main__':
    a = XMLAgent(address)
    a.join_switch(dpid1, port)
    a.join_link(dpid1, port1, dpid2, port2)